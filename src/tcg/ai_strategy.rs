use super::player::Card;
use super::player::Mana;
use super::strategy::Move;
use super::strategy::Strategy;

pub struct AiStrategy {}

impl Strategy for AiStrategy {
    fn next_move(
        &self,
        available_mana: Mana,
        _current_health: i32,
        available_cards: Vec<Card>,
    ) -> Move {
        self.highest_card(available_mana, available_cards)
    }
}
// public class AiStrategy extends Strategy {

//     @Override
//     public Move nextMove(int availableMana, int currentHealth, List<Card> availableCards) {
//         if (currentHealth < 10) {
//             return new Move(highestCard(availableMana, availableCards), Action.HEALING);
//         } else {
//             return new Move(bestCard(availableMana, availableCards), Action.DAMAGE);
//         }
//     }

//     private Optional<Card> bestCard(int availableMana, List<Card> availableCards) {
//         List<List<Card>> cardCombos = new ArrayList<>();
//         List<Card> remainingCards = new ArrayList<>(availableCards);
//         remainingCards.sort(Comparator.<Card>reverseOrder()); // highest mana costs first
//         while (!remainingCards.isEmpty()) {
//             List<Card> selectedCards = new ArrayList<>();
//             collectMaxDamageCardCombo(selectedCards, availableMana, remainingCards);
//             cardCombos.add(selectedCards);
//             remainingCards.remove(0);
//         }

//         List<Card> bestCombo = new ArrayList<>();
//         int maxDamage = 0;
//         for (List<Card> combo : cardCombos) {
//             int comboDamage = combo.stream().mapToInt(Card::getValue).sum();
//             if (comboDamage > maxDamage || (comboDamage == maxDamage && combo.size() > bestCombo.size())) {
//                 maxDamage = comboDamage;
//                 bestCombo = combo;
//             }
//         }

//         return bestCombo.stream().max(Comparator.<Card>naturalOrder());
//     }

//     private void collectMaxDamageCardCombo(List<Card> selectedCards, int availableMana, List<Card> availableCards) {
//         for (Card card : availableCards) {
//             List<Card> remainingCards = new ArrayList<>(availableCards);
//             if (selectedCards.stream().mapToInt(Card::getValue).sum() + card.getValue() <= availableMana) {
//                 selectedCards.add(card);
//                 remainingCards.remove(card);
//                 collectMaxDamageCardCombo(selectedCards, availableMana - card.getValue(), remainingCards);
//             }
//         }
//     }

// }

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_highst_value_card() {
        let strategy = AiStrategy {};
        let next_move = strategy.highest_card(4, vec![1, 3, 2, 1]);
        assert!(next_move.card.is_some());
        assert_eq!(next_move.card.unwrap(), 3);
    }

    #[test]
    fn get_lowest_value_card() {
        let strategy = AiStrategy {};
        let next_move = strategy.lowest_card(4, vec![3, 2, 1]);
        assert!(next_move.card.is_some());
        assert_eq!(next_move.card.unwrap(), 1);
    }

    #[test]
    fn should_return_none_if_mana_is_insufficient() {
        let strategy = AiStrategy {};
        let next_move = strategy.next_move(4, 30, vec![5, 6, 7]);
        assert!(next_move.card.is_none());
    }

    #[test]
    fn should_play_the_most_value_card_when_no_combo_is_possible() {
        let strategy = AiStrategy {};
        let next_move = strategy.next_move(2, 30, vec![3, 2, 1]);
        assert!(next_move.card.is_some());
        assert_eq!(next_move.card.unwrap(), 2);
    }
}

// public class AiStrategyTest {

//     @Test
//     public void shouldMaximizeDamageOutputInCurrentTurn() {
//         assertThat(strategy.nextMove(withMana(8), andHealth(30), fromCards(7, 6, 4, 3, 2)), either(is(isAttackingWithCard(2))).or(is(isAttackingWithCard(6))));
//     }

//     @Test
//     public void shouldPlayAsManyCardsAsPossibleForMaximumDamage() {
//         assertThat(strategy.nextMove(withMana(3), andHealth(30), fromCards(1, 2, 3)), either(is(isAttackingWithCard(1))).or(is(isAttackingWithCard(2))));
//     }

//     @Test
//     public void shouldPickHighestAffordableCardWhenNoComboIsPossible() {
//         assertThat(strategy.nextMove(withMana(2), andHealth(30), fromCards(1, 2, 3)), isAttackingWithCard(2));
//     }

//     @Test
//     public void shouldUseHealingUntilHealthIsAtLeast10() {
//         assertThat(strategy.nextMove(withMana(3), andHealth(8), fromCards(1, 1, 1)), isHealingWithCard(1));
//         assertThat(strategy.nextMove(withMana(2), andHealth(9), fromCards(1, 1)), isHealingWithCard(1));
//         assertThat(strategy.nextMove(withMana(1), andHealth(10), fromCards(1)), isAttackingWithCard(1));
//     }
// }
