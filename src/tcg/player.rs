use std::{error, fmt};

extern crate rand;
use rand::seq::SliceRandom;
use rand::thread_rng;

#[derive(Debug, Clone)]
pub enum PlayerError {
    CardNotFound,
    NotEnoughMana,
}

impl fmt::Display for PlayerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PlayerError::CardNotFound => write!(f, "card was not found!"),
            PlayerError::NotEnoughMana => write!(f, "insufficient mana!"),
        }
    }
}

impl error::Error for PlayerError {
    fn description(&self) -> &str {
        match self {
            PlayerError::CardNotFound => "card was not found!",
            PlayerError::NotEnoughMana => "insufficient mana!",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        None
    }
}

pub type Card = u8;
pub type Mana = u32;

#[derive(Debug, Clone)]
pub struct Player {
    pub health: i32,

    pub mana_slots: u32,
    pub mana: Mana,

    pub deck: Vec<Card>,
    pub hand: Vec<Card>,
}

impl Player {
    pub fn draw_card(&mut self) {
        match self.deck.pop() {
            None => {
                self.take_damage(1);
            }
            Some(card) => {
                if self.hand.len() < 5 {
                    self.hand.push(card);
                }
            }
        }
    }

    pub fn hand_size(&self) -> usize {
        self.hand.len()
    }

    pub fn shuffle_deck(&mut self) {
        let mut rng = thread_rng();
        self.deck.shuffle(&mut rng);
    }

    pub fn take_damage(&mut self, damage: i32) {
        self.health -= damage;
    }

    pub fn can_play_cards(self) -> bool {
        let number_of_playables = self
            .hand
            .iter()
            .filter(|&n| u32::from(*n) <= self.mana)
            .count();
        number_of_playables > 0
    }

    pub fn play_card(
        &mut self,
        card: Card,
        opponent: &mut Player,
    ) -> std::result::Result<(), PlayerError> {
        match self.hand.iter().position(|x| *x == card) {
            None => Err(PlayerError::CardNotFound),
            Some(index) => {
                if self.mana < u32::from(card) {
                    return Err(PlayerError::NotEnoughMana);
                }

                self.hand.remove(index);
                self.mana -= u32::from(card);
                opponent.health -= i32::from(card);
                Ok(())
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct PlayerBuilder {
    health: i32,
    mana_slots: u32,
    mana: Mana,
    deck: Vec<Card>,
    hand: Vec<Card>,
}

impl Default for PlayerBuilder {
    fn default() -> PlayerBuilder {
        PlayerBuilder {
            health: 30,
            mana_slots: 0,
            mana: 0,
            deck: vec![0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7, 8],
            hand: vec![],
        }
    }
}

impl PlayerBuilder {
    pub fn with_health(mut self, health: i32) -> Self {
        self.health = health;
        self
    }

    pub fn with_mana(mut self, mana: Mana) -> Self {
        self.mana = mana;
        self
    }

    pub fn with_mana_slots(mut self, slots: u32) -> Self {
        self.mana_slots = slots;
        self
    }

    pub fn with_deck(mut self, deck: Vec<Card>) -> Self {
        self.deck = deck;
        self
    }

    pub fn with_hand(mut self, hand: Vec<Card>) -> Self {
        self.hand = hand;
        self
    }

    pub fn build(self) -> Player {
        Player {
            health: self.health,
            mana_slots: self.mana_slots,
            mana: self.mana,
            deck: self.deck,
            hand: self.hand,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_player_should_have_30_initial_health() {
        let player = PlayerBuilder::default().build();
        assert_eq!(player.health, 30);
    }

    #[test]
    fn default_player_should_have_0_initial_mana() {
        let player = PlayerBuilder::default().build();
        assert_eq!(player.mana_slots, 0);
    }

    #[test]
    fn default_player_should_start_with_empty_hand() {
        let player = PlayerBuilder::default().build();
        assert!(player.hand.is_empty());
    }

    #[test]
    fn default_player_should_start_with_20_cards_deck() {
        let player = PlayerBuilder::default().build();
        assert_eq!(player.deck.len(), 20);
    }

    #[test]
    fn drawing_should_move_a_card_from_deck_to_hand() {
        let mut player = PlayerBuilder::default().with_deck(vec![0, 1, 2]).build();

        player.draw_card();

        assert_eq!(player.deck.len(), 2);
        assert_eq!(player.hand.len(), 1);
    }

    #[test]
    fn drawing_in_empty_deck_should_cause_damage() {
        let mut player = PlayerBuilder::default()
            .with_deck(vec![])
            .with_health(10)
            .build();

        player.draw_card();

        assert!(player.deck.is_empty());
        assert!(player.hand.is_empty());
        assert_eq!(player.health, 9);
    }

    #[test]
    fn player_should_discard_if_more_then_5_cards_in_hand() {
        let mut player = PlayerBuilder::default()
            .with_deck(vec![1])
            .with_hand(vec![1, 2, 3, 4, 5])
            .build();

        player.draw_card();

        assert!(player.deck.is_empty());
        assert_eq!(player.hand.len(), 5);
    }

    #[test]
    fn playing_cards_should_reduce_mana() {
        let mut player = PlayerBuilder::default()
            .with_mana(10)
            .with_hand(vec![8, 1])
            .build();

        let mut opponent = PlayerBuilder::default().build();

        let _ = player.play_card(8, &mut opponent);
        let _ = player.play_card(1, &mut opponent);

        assert_eq!(player.mana, 1);
    }

    #[test]
    fn playing_cards_should_remove_cards_from_hand() {
        let mut player = PlayerBuilder::default()
            .with_mana(5)
            .with_hand(vec![0, 2, 2, 3])
            .build();

        let mut opponent = PlayerBuilder::default().build();

        let _ = player.play_card(3, &mut opponent);
        let _ = player.play_card(2, &mut opponent);

        let card_with_value_2 = player.hand.iter().filter(|&n| *n == 2).count();
        let card_with_value_3 = player.hand.iter().filter(|&n| *n == 3).count();

        assert_eq!(card_with_value_2, 1);
        assert_eq!(card_with_value_3, 0);
    }

    #[test]
    fn playing_cards_should_causes_damage_to_opponent() {
        let mut player = PlayerBuilder::default()
            .with_mana(10)
            .with_hand(vec![3, 2])
            .build();

        let mut opponent = PlayerBuilder::default().with_health(10).build();

        let _ = player.play_card(3, &mut opponent);
        let _ = player.play_card(2, &mut opponent);

        assert_eq!(opponent.health, 5);
    }

    #[test]
    fn player_with_sufficient_mana_can_play_cards() {
        let player = PlayerBuilder::default()
            .with_mana(2)
            .with_hand(vec![3, 2])
            .build();
        assert!(player.can_play_cards())
    }

    #[test]
    fn player_with_insufficient_mana_can_not_play_cards() {
        let player = PlayerBuilder::default()
            .with_mana(1)
            .with_hand(vec![3, 2])
            .build();
        assert!(!player.can_play_cards())
    }

    #[test]
    fn player_with_empty_hand_can_not_play_cards() {
        let player = PlayerBuilder::default().with_hand(vec![]).build();
        assert!(!player.can_play_cards())
    }

    #[test]
    fn playing_card_with_insufficient_mana_should_fail() {
        let mut player = PlayerBuilder::default()
            .with_mana(3)
            .with_hand(vec![4, 4, 4])
            .build();

        let mut opponent = PlayerBuilder::default().with_health(10).build();

        let play_result = player.play_card(4, &mut opponent);
        assert!(play_result.is_err());
    }

    // @Test(expected = IllegalMoveException.class)
    // public void playingCardShouldFailWhenStrategyCannotChooseCard() {
    //     given(strategy.nextMove(anyInt(), anyInt(), anyListOf(Card.class))).willReturn(noMove());
    //     player.playCard(anyPlayer());
    // }

    // @Test
    // public void playingCardAsHealingRestoresHealth() {
    //     player = aPlayer().withHealth(10).withMana(10).withCardsInHand(3, 4).build();

    //     player.playCard(aCardWithValue(3), anyPlayer(), HEALING);
    //     player.playCard(aCardWithValue(4), anyPlayer(), HEALING);

    //     assertThat(player.getHealth(), is(17));
    // }

    // @Test
    // public void playerCannotHealAbove30Health() {
    //     player = aPlayer().withHealth(27).withMana(10).withCardsInHand(4).build();

    //     player.playCard(aCardWithValue(4), anyPlayer(), HEALING);

    //     assertThat(player.getHealth(), is(30));
    // }
}
