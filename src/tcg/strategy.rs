use super::player::Card;
use super::player::Mana;

pub struct Move {
    pub card: Option<Card>,
}

pub trait Strategy {
    fn next_move(
        &self,
        available_mana: Mana,
        current_health: i32,
        available_cards: Vec<Card>,
    ) -> Move;

    fn highest_card(&self, available_mana: Mana, available_cards: Vec<Card>) -> Move {
        let mut max_value_card: Option<Card> = None;

        for card in available_cards {
            if u32::from(card) <= available_mana {
                if max_value_card.is_none()
                    || (max_value_card.is_some() && (max_value_card.unwrap() < card))
                {
                    max_value_card = Some(card);
                }
            }
        }

        Move {
            card: max_value_card,
        }
    }

    fn lowest_card(&self, available_mana: Mana, available_cards: Vec<Card>) -> Move {
        let mut min_value_card: Option<Card> = None;

        for card in available_cards {
            if u32::from(card) <= available_mana {
                if min_value_card.is_none()
                    || (min_value_card.is_some() && (min_value_card.unwrap() > card))
                {
                    min_value_card = Some(card);
                }
            }
        }

        Move {
            card: min_value_card,
        }
    }
}
