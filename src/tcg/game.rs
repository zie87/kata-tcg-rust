use super::player::Player;
use super::player::PlayerBuilder;

use std::cell::RefCell;
use std::rc::Rc;

struct Game {
    player_1: Rc<RefCell<Player>>,
    player_2: Rc<RefCell<Player>>,
    first_is_active: bool,
}

impl Game {
    fn begin_turn(&self) {
        let active_player = self.get_active_player();
        active_player.borrow_mut().mana_slots += 1;
        let current_slots = active_player.borrow().mana_slots;

        active_player.borrow_mut().mana = current_slots;
        active_player.borrow_mut().draw_card();
    }

    fn end_turn(&mut self) {
        self.first_is_active = !self.first_is_active;
    }

    fn get_active_player(&self) -> Rc<RefCell<Player>> {
        if self.first_is_active {
            return Rc::clone(&self.player_1);
        }
        Rc::clone(&self.player_2)
    }

    fn get_opponent_player(&self) -> Rc<RefCell<Player>> {
        if !self.first_is_active {
            return Rc::clone(&self.player_1);
        }
        Rc::clone(&self.player_2)
    }

    fn has_winner(&self) -> bool {
        self.get_winner().is_some()
    }

    fn get_winner(&self) -> Option<Rc<RefCell<Player>>> {
        if self.player_1.borrow().health < 1 {
            return Some(Rc::clone(&self.player_2));
        }

        if self.player_2.borrow().health < 1 {
            return Some(Rc::clone(&self.player_1));
        }
        None
    }
}

#[derive(Debug, Clone)]
struct GameBuilder {
    player_1: Rc<RefCell<Player>>,
    player_2: Rc<RefCell<Player>>,
    first_is_active: bool,
}

impl Default for GameBuilder {
    fn default() -> GameBuilder {
        GameBuilder {
            player_1: Rc::new(RefCell::new(PlayerBuilder::default().build())),
            player_2: Rc::new(RefCell::new(PlayerBuilder::default().build())),
            first_is_active: true,
        }
    }
}

impl GameBuilder {
    fn with_players(mut self, p_1: Player, p_2: Player) -> Self {
        self.player_1 = Rc::new(RefCell::new(p_1));
        self.player_2 = Rc::new(RefCell::new(p_2));
        self
    }

    fn first_player_is_active(mut self) -> Self {
        self.first_is_active = true;
        self
    }

    fn second_player_is_active(mut self) -> Self {
        self.first_is_active = false;
        self
    }

    fn build(self) -> Game {
        let game = Game {
            player_1: self.player_1,
            player_2: self.player_2,
            first_is_active: self.first_is_active,
        };

        game.player_1.borrow_mut().shuffle_deck();
        for _ in 0..3 {
            game.player_1.borrow_mut().draw_card();
        }

        game.player_2.borrow_mut().shuffle_deck();
        for _ in 0..3 {
            game.player_2.borrow_mut().draw_card();
        }

        game.get_opponent_player().borrow_mut().draw_card();
        game
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn active_player_should_start_game_with_three_cards() {
        let game = GameBuilder::default().build();

        let active_player = game.get_active_player();
        assert_eq!(active_player.borrow().hand.len(), 3);
        assert_eq!(active_player.borrow().deck.len(), 17);
    }

    #[test]
    fn opponent_player_should_start_game_with_four_cards() {
        let game = GameBuilder::default().build();

        let opponent_player = game.get_opponent_player();
        assert_eq!(opponent_player.borrow().hand.len(), 4);
        assert_eq!(opponent_player.borrow().deck.len(), 16);
    }

    #[test]
    fn active_player_should_switch_at_end_of_turn() {
        let mut game = GameBuilder::default().build();

        let active_player = game.get_active_player();
        game.end_turn();

        assert!(!std::ptr::eq(
            active_player.as_ptr(),
            game.get_active_player().as_ptr()
        ));
    }

    #[test]
    fn active_player_should_receive_one_mana_at_begin_of_turn() {
        let game = GameBuilder::default().build();

        let start_slots = game.get_active_player().borrow().mana_slots;
        game.begin_turn();
        let current_slots = game.get_active_player().borrow().mana_slots;
        assert_eq!(current_slots, (start_slots + 1));
    }

    #[test]
    fn active_player_should_have_mana_refilled_at_begin_of_turn() {
        let game = GameBuilder::default()
            .with_players(
                PlayerBuilder::default()
                    .with_mana_slots(3)
                    .with_mana(0)
                    .build(),
                PlayerBuilder::default().build(),
            )
            .first_player_is_active()
            .build();

        game.begin_turn();
        assert_eq!(
            game.get_active_player().borrow().mana_slots,
            game.get_active_player().borrow().mana
        );
    }

    #[test]
    fn active_player_should_draw_on_begin_of_turn() {
        let game = GameBuilder::default().build();
        let init_hand_size = game.get_active_player().borrow().hand_size();

        game.begin_turn();

        assert_eq!(
            game.get_active_player().borrow().hand_size(),
            (init_hand_size + 1)
        );
    }

    #[test]
    fn ongoing_game_has_no_winner() {
        let game = GameBuilder::default()
            .with_players(
                PlayerBuilder::default()
                    .with_mana(10)
                    .with_hand(vec![4, 6])
                    .build(),
                PlayerBuilder::default().with_health(30).build(),
            )
            .build();

        let active_player = game.get_active_player();
        let _ = active_player
            .borrow_mut()
            .play_card(4, &mut game.get_opponent_player().borrow_mut());

        assert!(!game.has_winner());
    }

    #[test]
    fn player_with_one_health_and_empty_deck_should_die_bleed_out_at_begin_of_turn() {
        let game = GameBuilder::default()
            .with_players(
                PlayerBuilder::default().build(),
                PlayerBuilder::default()
                    .with_health(1)
                    .with_deck(vec![])
                    .build(),
            )
            .second_player_is_active()
            .build();

        game.begin_turn();
        assert!(game.has_winner());

        let winner = game.get_winner().unwrap();
        assert!(std::ptr::eq(
            winner.as_ptr(),
            game.get_opponent_player().as_ptr()
        ));
    }

    #[test]
    fn opponent_loses_when_health_is_zero() {
        let game = GameBuilder::default()
            .with_players(
                PlayerBuilder::default()
                    .with_mana(10)
                    .with_hand(vec![4, 6])
                    .build(),
                PlayerBuilder::default().with_health(10).build(),
            )
            .build();

        let active_player = game.get_active_player();
        let _ = active_player
            .borrow_mut()
            .play_card(6, &mut game.get_opponent_player().borrow_mut());
        let _ = active_player
            .borrow_mut()
            .play_card(4, &mut game.get_opponent_player().borrow_mut());

        assert!(game.has_winner());

        let winner = game.get_winner().unwrap();
        assert!(std::ptr::eq(winner.as_ptr(), active_player.as_ptr()));
    }

}
